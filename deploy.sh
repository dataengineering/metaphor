#!/bin/sh

cp -p guidelines/guidelines.pdf web/doc/
rsync -av --delete metaphor:/web/stats/ web/stats
rsync -av --delete --exclude stats web/ metaphor:/web

exit 0